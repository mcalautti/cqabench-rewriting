
def convertType(sqlType):
	if sqlType == "numeric":
		return "double"
	elif sqlType == "integer":
		return "integer"
	else:
		return "string"
