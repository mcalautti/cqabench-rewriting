import json
import psycopg2

class dbConnection:
	def __init__(self, connectionPath):
		connection = {}
		with open(connectionPath) as json_file:  
		    connection = json.load(json_file)

		connection_string = "dbname= '" + connection["dbname"] + "' user= '" + connection["user"]+ "' host= '" + connection["host"] +"' password= '" + connection["password"]+ "'";
		print(connection_string)

		try:
		    self.conn = psycopg2.connect(connection_string)
		except:
		    raise Exception("I am unable to connect to the database")

