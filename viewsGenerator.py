class viewsGenerator:

	QUERY_TEMPLATE = """SELECT $ATTS_LIST$,  
	\'$TAB_NAME$\' AS _table, 
	DENSE_RANK() OVER (ORDER BY $KEY_LIST$) AS _bid,
	ROW_NUMBER() OVER (PARTITION BY $KEY_LIST$ ORDER BY $ANTI_KEY_LIST$) AS _tid,
	count(*) over (PARTITION BY $KEY_LIST$) AS _keyCount
	FROM $TAB_NAME$"""

	def listToString(self, l):
		res = ""
		for e in l:
			res += e +", ";
		return res[0:len(res)-2]

	def __init__(self, schema):
		self.schema = schema
		self.schemaName = schema['name']
		self.tableNames = []
		for t in schema['tables']:
			self.tableNames.append(t['name'])
	
	def getView(self, tabName, schemaName=None):

		if schemaName == None:
			schemaName = self.schemaName

		tab = None

		for t in self.schema["tables"]:
			if t['name'] == tabName:
				tab = t
				break
		if tab == None:
			print('Make Views: Cannot find table ' + str(tabName))
			exit()
		
		anti_key_list = []
		for r in tab["attributes"]:
			if r not in tab["key"]:
				anti_key_list.append(r)
		
		#The order of attributes changes in the views.
		#The new order is: KEY, ANTI-KEY
		q = self.QUERY_TEMPLATE.replace("$TAB_NAME$", schemaName + '.' +tab['name'])
		q = q.replace("$ATTS_LIST$", self.listToString(tab['attributes']))
		q = q.replace("$KEY_LIST$", self.listToString(tab['key']))
		q = q.replace("$ANTI_KEY_LIST$", self.listToString(anti_key_list))
		return q