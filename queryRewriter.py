from moz_sql_parser import parse
from moz_sql_parser import format

def rewrite(inQuery, viewSuffix):
	parsedQuery = parse(inQuery)

	if(isinstance(parsedQuery["from"], basestring)):
		parsedQuery["from"] = [parsedQuery["from"]]

	projList = parsedQuery["select"]
	if (isinstance(projList, dict)):
		projList = [projList]

	tabList = []
	parsedQuery["select"] = []

	#Changes tables into CQA views
	for i, tab in enumerate(parsedQuery["from"]):
		tabName = ""
		if isinstance(tab, basestring):
			tabName = tab
			parsedQuery["from"][i] = {"value" : tab+viewSuffix, "name" : tab}
			tabList.append(tab)
		elif isinstance(tab, dict) and isinstance(tab["value"], basestring):
			tabName = tab["value"]
			tab["value"] = tab["value"]+viewSuffix 
			tabList.append(tab["name"])
		else:
			raise Exception("Something unexpected in the from clause! Element: " + str(tab))

	if isinstance(projList, list):
		orderBy = []
		for p in projList:
			entry = {'value': p['value']}
			orderBy.append(entry)
			parsedQuery["select"].append(entry)
		parsedQuery['orderby'] = orderBy

	for t in tabList:
		parsedQuery["select"].append({'value' : t+'._table'})
		parsedQuery["select"].append({'value' : t+'._bid'})
		parsedQuery["select"].append({'value' : t+'._tid'})
		parsedQuery["select"].append({'value' : t+'._keyCount'})

	return format(parsedQuery)
